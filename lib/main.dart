import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import './screens/Home/home.dart';
import './config.dart';

// void main() => runApp(MaterialApp(
//   title: 'My Contacts App',
//   debugShowCheckedModeBanner: false,
//   theme: ThemeData.light(),
//   home: Home(),
// ));

void main() async => runApp(EasyLocalization(child: MyApp()));

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {

    // Data Localization
    var data = EasyLocalizationProvider.of(context).data;

    return EasyLocalizationProvider(
      data: data, // Date Locatization
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          EasylocaLizationDelegate(
              locale: data.locale ?? Locale('ar', 'DZ'),
              path: 'resources/langs'),
        ],
        supportedLocales: [Locale('ar', 'DZ')],
        locale: data.locale,
        debugShowCheckedModeBanner: false,
        theme: Config.lightTheme,
        home: Home(),
      ),
    );
  }
}
