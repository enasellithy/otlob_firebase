import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class CRUD {

  bool auth(){
    return FirebaseAuth.instance.currentUser() != null ? true:false;
  }

  Future<void> createProfile(data) async{
    if(auth()){
      Firestore.instance.collection('users').document().setData(data);
    }
  }

  Future<void> createrestaurant(data) async{
    if(auth()){
      Firestore.instance.collection('restaurants').document().setData(data);
      
    }
  }

  // getDataUser() async{
  //   FirebaseUser userData = await FirebaseAuth.instance.currentUser();
  //   return await Firestore.instance
  //   .collection('users')
  //   .where('uid',isEqualTo:userData.uid)
  //   .getDocuments();
  // }

  getProfile() async {
    return Firestore.instance.collection('users').where('uid',isEqualTo: FirebaseAuth.instance.currentUser()).snapshots();
  }

}