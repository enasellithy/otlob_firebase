import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import '../Login/login.dart';
import '../Singup/singup.dart';
import '../../main.dart';
import './CustomShapeClipper.dart';
import 'package:flutter/rendering.dart';
import './homeScreenBody.dart';
import '../../CRUD.dart';
import '../Profile/Profile.dart';
import '../Restaurant/ِAllRestaurant.dart';

class Home extends StatefulWidget {

  const Home({
    Key key,
    this.user
  }) : super(key:key);

  final FirebaseUser user;

  //@override
  _HomeState createState() => _HomeState();
}

Color firstColor = Colors.cyan[100];
Color secondColor = Colors.cyan[300];

List<String> locations = ['سوهاج', 'جرجا','ابحث عن مطعم او وجبتك '];

class _HomeState extends State<Home> {

  CRUD crud = new CRUD();

  QuerySnapshot getProfile;

  FirebaseUser user;
  final auth = FirebaseAuth.instance;
  Future<void> getUserData() async {
    FirebaseUser userData = await FirebaseAuth.instance.currentUser();
    setState(() {
      user = userData;
    });
  }

  @override
  void initState() { 
    super.initState();
    getUserData();
  }

  Future<void> _singout() async{
    auth.signOut();
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Login()));
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        
    drawer: Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Center(child: Text('اطلب في سوهاج ',style: TextStyle(fontSize: 24.0,color: Colors.white),)),
            decoration: BoxDecoration(
              color: Colors.cyan[300],
            
            ),
          ),
          ListTile(
            title: Text('الوجبات'),
            onTap: () {
    
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('المطاعم'),
            onTap: () {
    
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('افضل العروض'),
            onTap: () {
    
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('الاكثر طلبا'),
            onTap: () {
             
              Navigator.pop(context);
            },
          ),
          user == null ?
          Column(
            children: <Widget>[
              ListTile(
                title: Text('تسجيل الدخول'),
                onTap: () {
                  Navigator.push(context,new MaterialPageRoute(
                      builder: (context) => Login()));
                },
              ),
              ListTile(
                title: Text('تسجيل حساب جديد'),
                onTap: () {
                  Navigator.push(context,new MaterialPageRoute(
                      builder: (context) => Singup()));
                },
              ),
            ],
          ):
          Column(
            children: <Widget>[
                ListTile(
                  title: Text(' الملف الشخصى '),
                  onTap: () {
                      Navigator.push(context,new MaterialPageRoute(
                        builder: (context) => PrifleScreen()));
                },
              ),
              ListTile(
                  title: Text(' المطاعم '),
                  onTap: () {
                      Navigator.push(context,new MaterialPageRoute(
                        builder: (context) => AllRestaurant()));
                },
              ),
              ListTile(
                title: Text('تسجيل خروج'),
                onTap: _singout,
              ),
            ],
          ),
        ],
      ),
    ),
    body: ListView(
        children: <Widget>[
            HomeScreenContainer(), 
            HomeScreenBottomPart(),
            HomeScreenResturants(),     
        ],
      ),
    );
  }
}

class HomeScreenContainer extends StatefulWidget {
  @override
  _HomeScreenContainerState createState() => _HomeScreenContainerState();
}

const TextStyle dropDownLableStyle = TextStyle(color: Colors.white,fontSize: 14.0);
const TextStyle dropDownMenuStyle = TextStyle(color: Colors.black,fontSize: 18.0);

class _HomeScreenContainerState extends State<HomeScreenContainer> {
  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    final dataSize = MediaQuery.of(context).size;
    return EasyLocalizationProvider(
      data: data,
      child: Stack(
      children: <Widget>[
        ClipPath(
         clipper: CustomShapeClipper(),
          child: Container(height: 350.0,decoration: BoxDecoration(gradient: LinearGradient(colors: [
            firstColor,
            secondColor
          ],),),
          child: Column(
            children: <Widget>[
              
              SizedBox(height: 50.0,),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.menu, color: Colors.white),
                    Spacer(),
                    Icon(Icons.location_on, color: Colors.white),
                    SizedBox(width: 16.0,),
                    PopupMenuButton(
                      child: Row(
                        children: <Widget>[
                          Text(locations[0], style:dropDownLableStyle),
                          Icon(Icons.keyboard_arrow_down, color:Colors.white)
                        ],
                      ),
                      itemBuilder: (BuildContext context) => <PopupMenuItem<int>>[
                        PopupMenuItem(
                          child: Text(locations[0], style: dropDownMenuStyle),
                          value: 0,
                        ),
                        PopupMenuItem(
                          child: Text(locations[1], style: dropDownMenuStyle),
                          value: 1,
                        )
                      ],
                    ),                   
                  ],
                ),
              ),

              SizedBox(height: 50.0,),
              Text('اطلب في سوهاج', style: TextStyle(fontSize: 24.0, color: Colors.white),textAlign: TextAlign.center,),
              SizedBox(height: 30.0,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 32.0),
                child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  child: TextField(
                    controller: TextEditingController(text: locations[2]),
                    style: dropDownMenuStyle,
                    textAlign: TextAlign.right,
                    //cursorColor: appTheme.primaryColor,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 32.0,vertical: 14.0),
                      suffixIcon: Material(
                        elevation: 2.0,
                        borderRadius: BorderRadius.circular(30.0),
                        child: Icon(Icons.search, color: Colors.black),
                      ),
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ],
          ),
          ),
        )
      ],
      ),
    );
  }
}