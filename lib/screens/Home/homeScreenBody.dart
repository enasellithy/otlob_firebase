import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class HomeScreenBottomPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var viewAllStyle = TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold, color: Colors.orangeAccent);
    return Container(
      child:  Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(20.0),
          child:  Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 16.0,
            ),
            Text(
              "افضل العروض",
              style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold),
            ),
            Spacer(),
            Text(
              "عرض الكل",
              style: viewAllStyle,
            ),
          ],
        ),
        ),
       
       Container(
         height: 240.0,
         child: ListView(
           scrollDirection: Axis.horizontal,
           children: cards,
         ),
       ),

      ],
    ),
    );
  }
}

List<Card> cards = [
  Card("resources/images/340985196295e91891edd77ef5d769c6d366770e.jpg", "سمك مشوي", "ام حسن", "25", 120, 95),
  Card("resources/images/Albet El Zinger.png", "تشيكن برجر", "كنتاكي", "15", 150, 120),
  Card("resources/images/1481454181-IMG_3365.jpeg", "بيتزا فراخ", "بيتزا هوم", "10", 90, 80)
];

  final formatCurrency = new NumberFormat.simpleCurrency();

class Card extends StatelessWidget {
  final String imagePath, mealName, restName,discount; 
  final int oldPrice, newPrice;

  Card(this.imagePath, this.mealName, this.restName, this.discount, this.oldPrice, this.newPrice);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
    
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
            child: Stack(
              children: <Widget>[
                Container(
                  height: 210.0,
                  width: 160.0,
                  child: Image.asset(imagePath, fit: BoxFit.cover,),
                  ),

                  Positioned(
                    left: 0.0,
                    bottom: 0.0,
                    width: 160.0,
                    height: 60,
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          colors: [
                            Colors.black, Colors.black12
                          ]
                        )
                      ),
                    ),
                  ),

                  Positioned(
                    left: 10.0,
                    bottom: 10.0,
                    right: 10.0,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(mealName, 
                            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 14.0)),
                            Text(restName, 
                            style: TextStyle( color: Colors.white, fontSize: 12.0)),
                          ],
                        ),

                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 2.0),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0)
                            ),
                          ),
                          child: Text(
                            "$discount%",
                            style: TextStyle(fontSize: 14.0, color: Colors.black),
                          ),
                        ),

                      ],
                    ),
                  ),

              ],
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 5.0,),
              Text("${formatCurrency.format(newPrice)}", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16.0),),
              SizedBox(width: 5.0,),
              Text("${formatCurrency.format(oldPrice)}", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 14.0),),
            ],
          )
        ],
      ),
    );
  }
}

class HomeScreenResturants extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var viewAllStyle = TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold, color: Colors.orangeAccent);
    return Container(
      child:  Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(20.0),
          child:  Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 16.0,
            ),
            Text(
              "المطاعم",
              style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold),
            ),
            Spacer(),
            Text(
              "عرض الكل",
              style: viewAllStyle,
            ),
          ],
        ),
        ),
       Container(
         height: 200.0,
         child: ListView(
           scrollDirection: Axis.horizontal,
           children: rests,
         ),
       ),
      ],
    ),
    );
  }
}

List<Rest> rests = [
  Rest("resources/images/lasvegas.jpg", "ام حسن",),
  Rest("resources/images/KFCs-logo-since-2006.png", "كنتاكي",),
  Rest("resources/images/0eeaefbecab5.png","بيتزا هوم",)
];

class Rest extends StatelessWidget {
  final String imagePath, restName; 

  Rest(this.imagePath, this.restName,);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
    
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(
              Radius.circular(100.0),
            ),
            child: Stack(
              children: <Widget>[
                Container(
                  height: 140.0,
                  width: 140.0,
                  child: Image.asset(imagePath, fit: BoxFit.cover,),
                  ),
              ],
              
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            SizedBox(width: 10.0,),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 14.0),
                child: Text(restName, 
                    style: TextStyle( color: Colors.black, fontSize: 16.0,fontWeight: FontWeight.bold)),
              ),
            ),
            ]
          )
        ],
      ),
    );
  }
}