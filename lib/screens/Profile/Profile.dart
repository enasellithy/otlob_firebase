import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../CRUD.dart';

class PrifleScreen extends StatefulWidget {
  PrifleScreen({Key key}) : super(key: key);

  _PrifleScreenState createState() => _PrifleScreenState();
}

class _PrifleScreenState extends State<PrifleScreen> {

  CRUD crud = new CRUD();

  getDataUser() async{
    FirebaseUser userData = await FirebaseAuth.instance.currentUser();
    return await Firestore.instance
    .collection('users')
    .where('uid',isEqualTo:userData.uid)
    .getDocuments();
  }

  final GlobalKey<FormState> _profileForm = GlobalKey<FormState>();
  TextEditingController usernameInputController;
  TextEditingController pwdInputController;
  TextEditingController confirmPwdInputController;

  @override
  initState() {
    usernameInputController = new TextEditingController();
    pwdInputController = new TextEditingController();
    confirmPwdInputController = new TextEditingController();
    super.initState();
  }

   String pwdValidator(String value) {
    if (value.length < 5) {
      return 'Password must be longer than 5 characters';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;
    return EasyLocalizationProvider(
      data: data,
      child:Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 0.7,
          backgroundColor: Colors.white,
          title: Text(' تعديل الملف الشخصى ', textDirection: TextDirection.ltr, 
          style: TextStyle(color: Colors.cyan[300], fontFamily: 'Tajawal',
           fontWeight: FontWeight.bold),),),
    //       body: StreamBuilder<QuerySnapshot>(
    //   stream: Firestore.instance.collection('Categorie').snapshots(),
    //   builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    //     if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
    //     switch (snapshot.connectionState) {
    //       case ConnectionState.waiting:
    //         return new Center(
    //           child: Column(
    //             children: <Widget>[
    //               SizedBox(
    //                 child: CircularProgressIndicator(),
    //                 height: 50.0,
    //                 width: 50.0,
    //               ),
    //               Text('Dowload delle categorie...')
    //             ],
    //           ),
    //         );
    //       default:
    //         //_loadListOfCategories(snapshot);
    //         return new ListView(
    //           children:
    //               snapshot.data.documents.map((DocumentSnapshot document) {
    //             var nome = document['Nome'];
    //             print('doc: $nome');
    //             return new CategoryWidget(
    //               id: document.documentID,
    //               tap: onCategoryTap,
    //               nome: document['Nome'],
    //               count: document['Count'],
    //               checked: false,
    //             );
    //           }).toList(),
    //         );
    //     }
    //   },
    // ),
          //  body: StreamBuilder<QuerySnapshot>(
          //     stream: Firestore.instance.collection('users').
          //     where('uid',isEqualTo: FirebaseAuth.instance.currentUser()).snapshots(),
          //     builder: (context, snapshot) {
          //       if(snapshot.hasData){
          //         return new Text(snapshot.data.documents[0]['username']);
          //         // return Column(
          //         //   children: <Widget>[
          //         //       Hero(
          //         //         tag: 'Profile',
          //         //         child: snapshot.data.documents[0]['avatar'] ? CircleAvatar(
          //         //           backgroundColor: Colors.transparent,
          //         //           radius: 48.0,
          //         //           child: Image.network(snapshot.data.documents[0]['avatar']),
          //         //       ): 
          //         //       Container(
          //         //         color: Colors.cyan[300],
          //         //         child: Center(child: Icon(Icons.person, color: Colors.white,),),
          //         //       ),
          //         //     ),
          //         //     SizedBox(height: 48.0),
          //         //     Container(
          //         //       padding: EdgeInsets.all(10),
          //         //       child: Column(
          //         //         children: <Widget>[
          //         //           Form(
          //         //             key: _profileForm,
          //         //             child: Container(
          //         //                 child: Column(
          //         //                 ),
          //         //             ),
          //         //           ),
          //         //         ],
          //         //       ),
          //         //     ),
          //         //   ],
          //         // );
          //       }
          //     }
          // ),
          // body: StreamBuilder(
          //   stream: Firestore.instance
          //       .collection('users')
          //       .where('uid', isEqualTo: FirebaseAuth.instance.currentUser())
          //       .snapshots(),
          //   builder: (context, snapshot) {
          //     if (!snapshot.hasData) {
          //       return Text('Loading...');
          //     }
          //     return ListView.builder(
          //       itemCount: snapshot.data.documents.length,
          //       itemBuilder: (context, index) {
          //         return Container(
          //           height: 50,
          //           padding: EdgeInsets.only(
          //             left: 20,
          //           ),
          //           alignment: Alignment.centerLeft,
          //           //color: index % 2 == 0 ? Colors.yellow[100] : Colors.white,
          //           child: Text('${snapshot.data.documents[index]['username']}',
          //             style: TextStyle(fontSize: 20,),),
          //         );
          //       },
          //     );
          //   },
          // ),
      ),
    );
  }

//   Widget _handleCurrentScreen() {
//     return new StreamBuilder<FirebaseUser>(
//       stream: FirebaseAuth.instance.onAuthStateChanged,
//       builder: (BuildContext context, snapshot) {
//         if (snapshot.connectionState == ConnectionState.waiting) {
//           return new ;
//         } else {
//           if (snapshot.hasData) {
//             return new MainScreen(firestore: firestore,
//                 uuid: snapshot.data.uid);
//           }
//           return new LoginScreen();
//         }
//       }
//     );
// }


}