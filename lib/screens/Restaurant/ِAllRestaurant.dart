import 'package:firebase_app/screens/Restaurant/add_restaurant.dart';
import 'package:flutter/material.dart';
import '../../main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import '../Home/home.dart';
import '../Singup/singup.dart';
import './RestaurantCrud.dart';
import './ِAllRestaurant.dart';

class AllRestaurant extends StatefulWidget {
  AllRestaurant({Key key}) : super(key: key);
  _AllRestaurantState createState() => _AllRestaurantState();
}

class _AllRestaurantState extends State<AllRestaurant> {

  RestaurantCrud restCrud = new RestaurantCrud();

  QuerySnapshot restaurants;

  @override
  void initState() { 
    super.initState();
    restCrud.getData().then((data){
      setState(() {
        restaurants = data;
      });
    });
  }

  Widget showData(){
    if(restaurants != null && restaurants.documents != null){
      return ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 11.0, right: 11.0),
        itemCount: restaurants.documents.length,
        itemBuilder: (BuildContext context, index){
          return Container(
            child: Column(
              children: <Widget>[
                SizedBox(height: 8.0,),
                ListTile(
                      trailing: IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: (){
                          restaurants.documents[index].reference.delete();
                        },
                      ),
                      leading: IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: (){
                          // Navigator.push(context, MaterialPageRoute
                          //   (builder: (context) => UpdateContact(contacts.documents[index])
                          // ));
                        },
                      ),
                      title: Text('${restaurants.documents[index].data['name']}'),
                      subtitle: Text('${restaurants.documents[index].data['address']}'),
                    ),
              ],
            ),
          );
        }
      );
    } else if (restaurants != null && restaurants.documents.length == 0){
        return Text('No data right now');
      } else {
            return Text('Pleas');
          }
  }

  @override
  Widget build(BuildContext context) {

    var data = EasyLocalizationProvider.of(context).data;

    return EasyLocalizationProvider(
      data: data,
      child:Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 0.7,
          backgroundColor: Colors.white,
          title: Text(' المطاعم ', textDirection: TextDirection.ltr, 
          style: TextStyle(color: Colors.cyan[300], fontFamily: 'Tajawal',
          fontWeight: FontWeight.bold),),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add, color: Colors.cyan[300]),
              onPressed: () { Navigator.push(context, MaterialPageRoute(
                  builder: (context) => AddRestaurant())); },),
          ],
        ),
        body: showData(),
      ),
    );

  }
}