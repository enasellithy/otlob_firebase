import 'package:flutter/material.dart';
import '../../main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import '../Home/home.dart';
import './RestaurantCrud.dart';
import '../../Classes/User.dart';

class AddRestaurant extends StatefulWidget {
  AddRestaurant({Key key}) : super(key: key);
  _AddRestaurantState createState() => _AddRestaurantState();
}

class _AddRestaurantState extends State<AddRestaurant> {

  RestaurantCrud restCrud = new RestaurantCrud();

  DocumentSnapshot restUsers;

  // String _mySelection;


  // List data = List(); //edited line

  // Future<String> getSWData() async {
  //   var resBody = await Firestore.instance.collection('users').
  //   where('role',isEqualTo: 'restaurant').getDocuments();

  //   setState(() {
  //     data = resBody;
  //   });
  // }

  //QuerySnapshot _restUsers;

  //DocumentSnapshot users;

  // final db = Firestore.instance;

  // // RestUser({this.firestore});

  // // final Firestore firestore;
  // var _mySelection;

  final GlobalKey<FormState> _addRestaurant = GlobalKey<FormState>();
  TextEditingController nameController;
  TextEditingController addressController;
  TextEditingController descriptionController;
  TextEditingController imageController;
  TextEditingController userController;

  @override
  initState() {
    nameController = new TextEditingController();
    addressController = new TextEditingController();
    descriptionController = new TextEditingController();
    imageController = new TextEditingController();
    userController = new TextEditingController();
    super.initState();
    // _users = Firestore.instance.collection('users').
    // where('role',isEqualTo: 'restaurant').snapshots();
    // restCrud.getUsers().then((data){
    //   setState(() {
    //     users = data;
    //   });
    // });
  }

  @override
  Widget build(BuildContext context) {

    var data = EasyLocalizationProvider.of(context).data;

    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 0.7,
          backgroundColor: Colors.white,
          title: Text('  اضافه مطعم ', textDirection: TextDirection.ltr, 
          style: TextStyle(color: Colors.cyan[300], fontFamily: 'Tajawal',
           fontWeight: FontWeight.bold),),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.home, color: Colors.cyan[300],),
              onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));}
            ),
          ],
        ),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              SizedBox(height: 11.0),
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                      Form(
                        key: _addRestaurant,
                        autovalidate: true,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                controller: nameController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'اﻻسم',
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                  focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                  borderSide: BorderSide(color: Colors.cyan[300],)),
                                ),
                                validator: (val){
                                  if(val.isEmpty){
                                    return 'Please Enter Name';
                                  }
                                },
                              ),
                              SizedBox(height: 8.0),
                              TextFormField(
                                controller: addressController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'address',
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                  focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                  borderSide: BorderSide(color: Colors.cyan[300],)),
                                ),
                                validator: (val){
                                  if(val.isEmpty){
                                    return 'Please Enter address';
                                  }
                                },
                              ),
                              SizedBox(height: 8.0),
                              TextFormField(
                                controller: descriptionController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'description',
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                  focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                  borderSide: BorderSide(color: Colors.cyan[300],)),
                                ),
                                validator: (val){
                                  if(val.isEmpty){
                                    return 'Please Enter description';
                                  }
                                },
                              ),
                              SizedBox(height: 8.0,),

                              StreamBuilder<QuerySnapshot>(
                                stream: Firestore.instance
                                    .collection('users')
                                    .snapshots(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<QuerySnapshot> snapshot) {
                                  restUsers = snapshot.data.documents[0];
                                  // return DropdownButtonFormField<DocumentSnapshot>(
                                  //     decoration: InputDecoration(
                                  //       hintText: 'اﻻسم',
                                  //       contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                  //       focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                  //       borderSide: BorderSide(color: Colors.cyan[300],)),
                                  //     ),
                                  //     value: restUsers,
                                  //     onChanged: (DocumentSnapshot newValue) {
                                  //       setState(() {
                                  //         restUsers = newValue;
                                  //       });
                                  //       print(restUsers.data['username']);
                                  //     },
                                  //     onSaved: (DocumentSnapshot newValue) {
                                  //       setState(() {
                                  //         restUsers = newValue;
                                  //       });
                                  //     },
                                  //     items: snapshot.data.documents
                                  //         .map((DocumentSnapshot document) {
                                  //       return new DropdownMenuItem<DocumentSnapshot>(
                                  //         value: document.data["uid"],
                                  //         child: Text(
                                  //           //'${document.data["users"]}'
                                  //           document.data['users'],
                                  //         ),
                                  //       );
                                  //     }).toList(),
                                  //   );
                                  return DropdownButtonHideUnderline(
                                    child:new DropdownButtonFormField<DocumentSnapshot>(
                                      decoration: InputDecoration(
                                        hintText: 'اﻻسم',
                                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                        focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                        borderSide: BorderSide(color: Colors.cyan[300],)),
                                      ),
                                      value: restUsers,
                                      onChanged: (DocumentSnapshot newValue) {
                                        setState(() {
                                          restUsers = newValue;
                                        });
                                        print(restUsers.data['username']);
                                      },
                                      onSaved: (DocumentSnapshot newValue) {
                                        setState(() {
                                          restUsers = newValue;
                                        });
                                      },
                                      items: snapshot.data.documents
                                          .map((DocumentSnapshot document) {
                                        return new DropdownMenuItem<DocumentSnapshot>(
                                          value: document,
                                          child: Text(
                                            document.data['users'],
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  );
                              }),


                              // StreamBuilder<QuerySnapshot>(
                              //   stream: Firestore.instance.collection('users').snapshots(),
                              //   builder: (context, snapshot){
                              //       if(!snapshot.hasData){
                              //        return DropdownButtonFormField<DocumentSnapshot>(
                              //           decoration: InputDecoration(
                              //             hintText: 'description',
                              //             contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                              //             border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                              //             focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                              //             borderSide: BorderSide(color: Colors.cyan[300],)),
                              //           ),
                              //           items: <String>[],
                              //           // items: <String>[
                              //           //       'Sukhchayn Garden',
                              //           //       'Canal Garden',
                              //           //       'Bahria Town',
                              //           //     ].map((String value) {
                              //           //       return new DropdownMenuItem<String>(
                              //           //         value: value,
                              //           //         child: new Text(value),
                              //           //       );
                              //           //     }).toList(),
                              //           // onChanged:(DocumentSnapshot newValue) {
                              //           //   setState(() {
                              //           //     restUsers = newValue;
                              //           //   });
                              //           //   print(restUsers.data['username']);
                              //           // },
                              //           // validator: (restUsers) {
                              //           //   if (restUsers == null) {
                              //           //     return 'Users Not Selected';
                              //           //   }
                              //           // },
                              //           // onSaved: (DocumentSnapshot newValue) {
                              //           //   setState(() {
                              //           //     restUsers = newValue;
                              //           //   });
                              //           //   print(restUsers.data);
                              //           // },
                              //           // items: snapshot.data.documents.map((DocumentSnapshot document) {
                              //           //   return DropdownMenuItem<DocumentSnapshot>(
                              //           //       value: document,
                              //           //       child: new Text(document.data['username'],),
                              //           //   );
                              //           // }).toList(),
                              //         );
                              //       }
                              //   }
                              // ),

                              // StreamBuilder<QuerySnapshot>(
                              //   stream: Firestore.instance.collection('users').where('role',isEqualTo: 'restaurant').snapshots(),
                              //   builder: (context, snapshot){
                              //     if(!snapshot.hasData){
                              //       Text('Loading...');
                              //     }else{
                              //       List<DropdownMenuItem> restUser = [];
                              //       for(int i=0; i<snapshot.data.documents.length;i++){
                              //         DocumentSnapshot snap = snapshot.data.documents[i];
                              //         restUser.add(
                              //           DropdownMenuItem(
                              //             child: Text(snap['username']),
                              //             value: '${snap["uid"]}',
                              //           ),
                              //         );
                              //         // DropdownMenuItem(
                              //         //   child: Text(snapshot.data),
                              //         // );
                              //       }
                              //     }
                              //   },
                              // ),

                              // StreamBuilder<QuerySnapshot>(
                              //   //stream: db.collection('info').snapshots(),
                              //   stream: Firestore.instance.collection('users').where('role',isEqualTo: 'restaurant').snapshots(),
                              //   builder: (context, index) {
                              //     if (snapshot.hasData) {
                              //       return DropdownButtonFormField(
                              //         decoration: InputDecoration(
                              //           hintText: 'Users',
                              //           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                              //           border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                              //           focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                              //           borderSide: BorderSide(color: Colors.cyan[300],)),
                              //         ),
                              //         items: [
                              //           DropdownMenuItem<String>(
                              //             value: '${users[index].data['username']}',
                              //           ),
                              //         ],
                              //       );
                              //     }
                              //   }
                              // ),
                              //SizedBox(height: 8.0),
                              // SizedBox(
                              //   height: 8.0,
                              //   child:  new StreamBuilder<QuerySnapshot>(
                              //       stream: Firestore.instance.collection("users")
                              //       .where('role',isEqualTo: 'restaurant').snapshots(),
                              //       builder: (context, snapshot) {
                              //         if (!snapshot.hasData) return new Text("...");
                              //         var length = snapshot.data.documents.length;
                              //         DocumentSnapshot ds = snapshot.data.documents[length - 1];
                              //         return new DropdownButton(
                              //             items: snapshot.data.documents.map((
                              //                 DocumentSnapshot document) {
                              //               return DropdownMenuItem(
                              //                   value: document.data["uid"],
                              //                   child: new Text(document.data["username"]));
                              //             }).toList(),
                              //             value: _restUsers,
                              //             onChanged: (value) {
                              //               print(value);

                              //               setState(() {
                              //                   _restUsers = value;
                              //                 });
                              //             },
                              //             hint: new Text("Category"),
                              //             style: TextStyle(color: Colors.black),

                              //         );
                              //       }
                              //   ),
                              // ),
                              // DropdownButtonHideUnderline(
                              //   child: DropdownButton<String>(
                              //     hint: Text('Select'),
                              //     elevation: 4,
                              //     isDense: true,
                              //     isExpanded: true,
                              //     items: <String>[].map<DropdownMenuItem<String>>((String value){
                              //       return DropdownMenuItem<String>(
                              //         child: Text(value),
                              //         value: value,
                              //       );
                              //     }),
                              //   ),
                              // ),
                              // StreamBuilder<QuerySnapshot>(
                              //   stream: _users,
                              //   builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                              //     if(!snapshot.hasData){
                              //       return new Text("There is no expense");
                              //     }
                              //     return ListView(
                                    
                              //     );
                              //   }
                              // ),

                              // DropdownButtonFormField<String>(
                              //   //value: selected,
                              //   //users != null && users.documents != null ?
                              //   items: ["A", "B", "C"]

                              //       .map((label) => DropdownMenuItem(
                              //             child: Text(label),
                              //             value: label,
                              //           ))
                              //       .toList(),
                              //   onChanged: (value) {
                              //     //setState(() => selected = value);
                              //   },
                              // ),
                              // DropdownButtonFormField(
                              //   decoration: InputDecoration(
                              //     hintText: 'description',
                              //     contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                              //     border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                              //     focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                              //     borderSide: BorderSide(color: Colors.cyan[300],)),
                              //   ),
                                // items: [
                                //   DropdownMenuItem<String>(
                                //     value: '${users.documents.[index]}',
                                //   ),
                                // ],
                                // if(users != null && users.documents != null){
                                //     DropdownButton(),
                                //   }
                                // items: [
                                  
                                //   // DropdownMenuItem<String>(
                                //   //   value: '1',
                                //   //   child: Text('1'),
                                //   // ),${restaurants.documents[index].data['name']}
                                // ],
                              //),
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
    
  }
}