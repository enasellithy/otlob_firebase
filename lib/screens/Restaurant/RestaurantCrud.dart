import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class RestaurantCrud {

  bool auth(){
    return FirebaseAuth.instance.currentUser() != null ? true:false;
  }

  getUsers() async {
    return await Firestore.instance.collection('users').
    where('role',isEqualTo: 'restaurant').getDocuments();
  }

  Future<void> create(data) async{
    if(auth()){
      Firestore.instance.collection('restaurants').document().setData(data);
    }
  }

  getData() async{
    FirebaseUser userData = await FirebaseAuth.instance.currentUser();
    return await Firestore.instance.collection('restaurants').getDocuments();
    // return await Firestore.instance
    // .collection('restaurants')
    // .where('userId',isEqualTo:userData.uid)
    // .getDocuments();
  }

  update(data,docid) async{
    Firestore.instance
    .collection('restaurants')
    .document(docid)
    .setData(data);
  }

  delete(docid) async{
    Firestore.instance
    .collection('restaurants')
    .document(docid)
    .delete();
  }

}