import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../main.dart';
import '../Singup/singup.dart';
import '../Home/home.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  
  final GlobalKey<FormState> _formstate = GlobalKey<FormState>();
  String _email, _password;

  login() async{
    final formdate = _formstate.currentState;
    if(formdate.validate()){
      formdate.save();
      try{
          FirebaseUser fireuser = await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: _email, password: _password,
          );
          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => Home()),
            (Route<dynamic> route) => false);
      }catch(error){
        print(error.message);
      }
    }
  }
  
  @override
  Widget build(BuildContext context) {

    var data = EasyLocalizationProvider.of(context).data;

    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('resources/images/logo.png'),
      ),
    );

    final registerButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 4.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => Singup()
          ));
        },
        //padding: EdgeInsets.all(12),
        color: Colors.cyan[300],
        child: Text('تسجيل ', style: TextStyle(color: Colors.white,),),
      ),
    );

    return EasyLocalizationProvider(
      data: data,
      child: Scaffold(
      backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 0.7,
          backgroundColor: Colors.white,
          title: Text('تسجيل دخول', textDirection: TextDirection.ltr, 
          style: TextStyle(color: Colors.cyan[300], fontFamily: 'Tajawal', 
          fontWeight: FontWeight.bold),),),
        body: Center(
            child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 48.0),
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    Form(
                      key: _formstate,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                icon: Icon(Icons.email),
                                hintText: 'Email',
                                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                borderSide: BorderSide(color: Colors.cyan[300],)),
                              ),
                              validator: (val){
                                if(val.isEmpty){
                                  return 'Please Enter Your Email';
                                }
                              },
                              onSaved: (val) => _email = val,
                            ),
                            SizedBox(height: 8.0),
                            TextFormField(
                              decoration: InputDecoration(
                                icon: Icon(Icons.vpn_key),
                                hintText: 'Password',
                                focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                borderSide: BorderSide(color: Colors.cyan[300],)),
                                contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                              ),
                              obscureText: true,
                              validator: (val){
                                if(val.isEmpty){
                                  return 'Please Enter Password';
                                }else if(val.length < 3){
                                  return 'Your Password need to be atlesat 4 char';
                                }
                              },
                              onSaved: (val) => _password = val,
                            ),
                            Padding(
                            padding: EdgeInsets.symmetric(vertical: 16.0),
                            child:RaisedButton(
                                textColor: Colors.white,
                                //onPressed: login,
                                child: Text('تسجيل الدخول', style: TextStyle(color: Colors.white),),
                                onPressed: login,
                                shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24),),
                                padding: EdgeInsets.all(12),
                                color: Colors.cyan[300],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 8.0,),
                    Text(
                      "اذا لم يكن لديك حساب لدينا قم بالتسجيل من هنا"
                    ),
                    registerButton,
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}