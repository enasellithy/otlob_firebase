import 'package:flutter/material.dart';
import '../../main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import '../Home/home.dart';
import '../Singup/singup.dart';
import '../../CRUD.dart';

class Singup extends StatefulWidget {
  Singup({Key key}) : super(key: key);
  _SingupState createState() => _SingupState();
}

class _SingupState extends State<Singup> {

  CRUD crud = new CRUD();

  final GlobalKey<FormState> _registerFormKey = GlobalKey<FormState>();
  TextEditingController usernameInputController;
  TextEditingController emailInputController;
  TextEditingController pwdInputController;
  TextEditingController confirmPwdInputController;

  @override
  initState() {
    usernameInputController = new TextEditingController();
    emailInputController = new TextEditingController();
    pwdInputController = new TextEditingController();
    confirmPwdInputController = new TextEditingController();
    super.initState();
  }

  String emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return 'Email format is invalid';
    } else {
      return null;
    }
  }

  String pwdValidator(String value) {
    if (value.length < 5) {
      return 'Password must be longer than 5 characters';
    } else {
      return null;
    }
  }


  @override
  Widget build(BuildContext context) {
    var data = EasyLocalizationProvider.of(context).data;

    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('resources/images/logo.png'),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 4.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => Singup()
          ));
        },
        //padding: EdgeInsets.all(12),
        color: Colors.cyan[300],
        child: Text('لديك حساب ', style: TextStyle(color: Colors.white,),),
      ),
    );
    
    return EasyLocalizationProvider(
      data: data,
      child:Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          elevation: 0.7,
          backgroundColor: Colors.white,
          title: Text(' عضويه جديده ', textDirection: TextDirection.ltr, 
          style: TextStyle(color: Colors.cyan[300], fontFamily: 'Tajawal',
           fontWeight: FontWeight.bold),),),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 48.0),
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                    children: <Widget>[
                      Form(
                        key: _registerFormKey,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                controller: usernameInputController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  hintText: 'اسم المستخدم',
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                  focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                  borderSide: BorderSide(color: Colors.cyan[300],)),
                                ),
                                validator: (val){
                                  if(val.isEmpty){
                                    return 'Please Enter Your username';
                                  }
                                },
                              ),
                              SizedBox(height: 8.0),
                              TextFormField(
                                controller: emailInputController,
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  hintText: 'البريد اﻻلكترونى',
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                  focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                  borderSide: BorderSide(color: Colors.cyan[300],)),
                                ),
                                validator: emailValidator,
                              ),
                              SizedBox(height: 8.0),
                              TextFormField(
                                controller: pwdInputController,
                                decoration: InputDecoration(
                                  hintText: 'كلمه السر',
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                  focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                  borderSide: BorderSide(color: Colors.cyan[300],)),
                                ),
                                obscureText: true,
                                validator: pwdValidator,
                              ),
                              SizedBox(height: 8.0),
                              TextFormField(
                                controller: confirmPwdInputController,
                                decoration: InputDecoration(
                                  hintText: 'كلمه السر',
                                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                                  focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                  borderSide: BorderSide(color: Colors.cyan[300],)),
                                ),
                                obscureText: true,
                                validator: pwdValidator,
                              ),
                              SizedBox(height: 24.0),
                              Padding(
                              padding: EdgeInsets.symmetric(vertical: 16.0),
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(24),
                                  ),
                                  padding: EdgeInsets.all(12),
                                  color: Colors.cyan[300],
                                  child: Text(' تسجيل ', style: TextStyle(color: Colors.white)),
                                  onPressed: () async {
                                    if (_registerFormKey.currentState.validate()) {
                                      if (pwdInputController.text ==
                                            confirmPwdInputController.text) {
                                              FirebaseUser fireuser = await FirebaseAuth.instance.createUserWithEmailAndPassword(
                                              email: emailInputController.text, password: pwdInputController.text);
                                              debugPrint('${fireuser}');
                                              crud.createProfile({
                                                  "uid": fireuser.uid,
                                                  "username": usernameInputController.text,
                                                  "email": emailInputController.text,
                                                  "role": "user",
                                                  "avatar": "avater.png",
                                              });
                                              Navigator.of(context).pushAndRemoveUntil(
                                                MaterialPageRoute(builder: (context) => Home()),
                                                (Route<dynamic> route) => false);
                                              // if(fireuser != null) {
                                              //   return  Firestore.instance.collection('users').document(fireuser.uid).
                                              //   setData({
                                              //     "uid": fireuser.uid,
                                              //     "username": usernameInputController.text,
                                              //     "email": emailInputController.text,
                                              //     "role": "user",
                                              //     "avatar": "avater.png",
                                              //   });
                                              // }
                                              Navigator.of(context).pushAndRemoveUntil(
                                                MaterialPageRoute(builder: (context) => Home()),
                                                (Route<dynamic> route) => false);
                                      }else {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: Text("Error"),
                                                  content: Text("The passwords do not match"),
                                                  actions: <Widget>[
                                                    FlatButton(
                                                      child: Text("Close"),
                                                      onPressed: () {
                                                        Navigator.of(context).pop();
                                                      },
                                                    )
                                                  ],
                                                );
                                              });
                                        }
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0,),
                      Text(
                        " اذا كان لديك حساب قم يتسجيل الدخول "
                      ),
                      loginButton,
                    ],
                  ),
                ),
              ],
          ),
        ),
      ),
    );
  }
}

// class Singup extends StatefulWidget {
//   Singup({Key key}) : super(key: key);
//   _SingupState createState() => _SingupState();
// }

// class _SingupState extends State<Singup> {
//   final GlobalKey<FormState> _formstate = GlobalKey<FormState>();
//   String _email, _password;

//   Future<void> singup() async{
//     final formdate = _formstate.currentState;
//     if(formdate.validate()){
//       formdate.save();
//           FirebaseUser fireuser = await FirebaseAuth.instance.createUserWithEmailAndPassword(
//             email: _email, password: _password,
//           );
//           fireuser.sendEmailVerification();

//           Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(builder: (context) => Home()),
//             (Route<dynamic> route) => false);
//     }
//   }
  
//   @override
//   Widget build(BuildContext context) {
//     var data = EasyLocalizationProvider.of(context).data;

//     final logo = Hero(
//       tag: 'hero',
//       child: CircleAvatar(
//         backgroundColor: Colors.transparent,
//         radius: 48.0,
//         child: Image.asset('resources/images/logo.png'),
//       ),
//     );

//     final loginButton = Padding(
//       padding: EdgeInsets.symmetric(vertical: 4.0),
//       child: RaisedButton(
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(24),
//         ),
//         onPressed: () {
//           Navigator.push(context, MaterialPageRoute(
//             builder: (context) => Singup()
//           ));
//         },
//         //padding: EdgeInsets.all(12),
//         color: Colors.cyan[300],
//         child: Text('لديك حساب ', style: TextStyle(color: Colors.white,),),
//       ),
//     );
    
//     return EasyLocalizationProvider(
//       data: data,
//       child:Scaffold(
//         backgroundColor: Colors.white,
//         appBar: AppBar(
//           iconTheme: IconThemeData(color: Colors.black),
//           elevation: 0.7,
//           backgroundColor: Colors.white,
//           title: Text(' عضويه جديده ', textDirection: TextDirection.ltr, 
//           style: TextStyle(color: Colors.cyan[300], fontFamily: 'Tajawal',
//            fontWeight: FontWeight.bold),),),
//         body: Center(
//           child: ListView(
//             shrinkWrap: true,
//             padding: EdgeInsets.only(left: 24.0, right: 24.0),
//             children: <Widget>[
//               logo,
//               SizedBox(height: 48.0),
//               Container(
//                 padding: EdgeInsets.all(10),
//                 child: Column(
//                     children: <Widget>[
//                       Form(
//                         key: _formstate,
//                         child: Container(
//                           child: Column(
//                             children: <Widget>[
//                               TextFormField(
//                                 keyboardType: TextInputType.emailAddress,
//                                 decoration: InputDecoration(
//                                   hintText: 'Email Address',
//                                   contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//                                   border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
//                                   focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
//                                   borderSide: BorderSide(color: Colors.cyan[300],)),
//                                 ),
//                                 validator: (val){
//                                   if(val.isEmpty){
//                                     return 'Please Enter Your Email';
//                                   }
//                                 },
//                                 onSaved: (val) => _email = val,
//                               ),
//                               SizedBox(height: 8.0),
//                               TextFormField(
//                                 decoration: InputDecoration(
//                                   hintText: 'كلمه السر',
//                                   contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//                                   border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
//                                   focusedBorder:  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
//                                   borderSide: BorderSide(color: Colors.cyan[300],)),
//                                 ),
//                                 obscureText: true,
//                                 validator: (val){
//                                   if(val.isEmpty){
//                                     return 'Please Enter Password';
//                                   }else if(val.length < 3){
//                                     return 'Your Password need to be atlesat 4 char';
//                                   }
//                                 },
//                                 onSaved: (val) => _password = val,
//                               ),
//                               SizedBox(height: 24.0),
//                               Padding(
//                               padding: EdgeInsets.symmetric(vertical: 16.0),
//                               child: RaisedButton(
//                                 shape: RoundedRectangleBorder(
//                                   borderRadius: BorderRadius.circular(24),
//                                 ),
//                                 padding: EdgeInsets.all(12),
//                                 color: Colors.cyan[300],
//                                 child: Text(' تسجيل ', style: TextStyle(color: Colors.white)),
//                                         onPressed: singup,
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                       SizedBox(height: 8.0,),
//                       Text(
//                         " اذا كان لديك حساب قم يتسجيل الدخول "
//                       ),
//                       loginButton,
//                     ],
//                   ),
//                 ),
//               ],
//           ),
//         ),
//       ),
//     );
//   }
// }