import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class CrudFire {

  bool auth(){
    return FirebaseAuth.instance.currentUser() != null ? true:false;
  }
  

  Future<void> create(data) async{
    if(auth()){
      //Firestore.instance.collection('contacts').add(data);

      // Firestore.instance.runTransaction((Transaction ctrans) async{
      //   CollectionReference reference = await Firestore.instance.collection('contact');
      //   reference.add(data);
      // });

      Firestore.instance.collection('contacts').document().setData(data);
      
    }
  }

  getData() async{
    FirebaseUser userData = await FirebaseAuth.instance.currentUser();
    return await Firestore.instance
    .collection('contacts')
    .where('userId',isEqualTo:userData.uid)
    .getDocuments();
  }

  update(data,docid) async{
    Firestore.instance
    .collection('contacts')
    .document(docid)
    .setData(data);
  }

  delete(docid) async{
    Firestore.instance
    .collection('contacts')
    .document(docid)
    .delete();
  }

}