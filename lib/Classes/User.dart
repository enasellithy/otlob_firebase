import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';

class User {

  final String username;
  final int uid;

  const User({this.username, this.uid});

}