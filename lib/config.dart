import 'package:flutter/material.dart';

class Config {
  static String appName = 'اطلب في سوهاج';

  static Color pinkColor = Colors.pink[600];
  static Color greyColor = Colors.grey[50];
  static Color greyfont = Colors.grey[400];

  static ThemeData lightTheme = ThemeData(
    backgroundColor: greyColor,
    primaryColor: Color(0xFFF3791A),
    accentColor: greyfont,
    cursorColor: greyfont,
    scaffoldBackgroundColor: greyColor,
    fontFamily: 'Tajawal',
    iconTheme: new IconThemeData(color: greyColor),
    appBarTheme: AppBarTheme(
      textTheme: TextTheme(
        title: TextStyle(
          color: greyfont,
        )
      ),
    ),
  );

}